import React from 'react';
import { useState } from 'react';
import { Typography, Card, CardMedia, CardContent, CardActions, Button} from '@mui/material';
import { Dialog, DialogTitle, DialogContent, DialogActions, Alert } from '@mui/material';

const DeleteCard = ({id, nombre, descripcion, imagen}) => {
  const img = `data:image/jpeg;base64,${imagen}`;
  const [open, setOpen] = useState(false);
  const [actualCard, setCard] = useState(0);
  const [message, setMessage] = useState('');
  const [messageError, setMessageE] = useState(null);

  const handleDelete = (id) => {
    setOpen(true); 
    setCard(id);
  };

  const handleClose = () => {
    setOpen(false); 
  };

  const handleConfirmDelete =  async () => {

    fetch(`http://35.238.246.148:8023/entereza/eliminarEmpresa?idEmpresa=${actualCard}`, {
    method: 'DELETE'
    })
    .then(response => response.json())
    .then(data => {
    //empresa eliminada
    setMessage(data.msg);
    setTimeout(() => {
        window.location.reload();
      }) 
    })
    .catch(error => {
    // Error al crear
    setMessageE(error);
    });
    setOpen(false);
  };

  return(
    <div>
      <Card key={id} sx={{maxWidth:'400px'}}>
          <CardMedia component="img" height="140" image={img} />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {nombre}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {descripcion}
            </Typography>
          </CardContent>
              
            <CardActions>
            <Button size="small" color='error' onClick={() => handleDelete(id)}>Eliminar</Button>
            </CardActions> 

        <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Confirmar Eliminación</DialogTitle>
        <DialogContent>
        <Typography variant="body1">
        ¿Estás seguro de que deseas eliminar esta tarjeta?
        </Typography>
        </DialogContent>
        <DialogActions>
        <Button onClick={handleClose}>Cancelar</Button>
        <Button onClick={handleConfirmDelete} color="error" autoFocus>Eliminar</Button>
        </DialogActions>
        </Dialog>
        </Card>
    </div>
  );
}

export default DeleteCard;