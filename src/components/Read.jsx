import React from 'react';
import { Typography, CircularProgress, Grid, IconButton } from '@mui/material';
import { useEffect, useState } from 'react';
import CardComponent from './CardComponent';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

const Read = () =>{
  const [empresas, setEmpresas] = useState([]);
  const [page, setPage] = useState(0);
  const [size, setSize] = useState(6);
  const [loading, setLoading] = useState(true);
  const [nextPage, setNext] = useState(1);
  const [nextEmpresas, setNextEmpresas] = useState([]);

  const fetchEmpresas = async () => {
    try {
      const response = await fetch(`http://35.238.246.148:8023/entereza/listaEmpresas?page=${page}&size=${size}`);
      const data = await response.json();
      setEmpresas(data.empresas);
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  const checkNext = async () => {
    setLoading(true);
    try {
      const response = await fetch(`http://35.238.246.148:8023/entereza/listaEmpresas?page=${nextPage}&size=${size}`);
      const data = await response.json();
      setNextEmpresas(data.empresas);
      console.log("datos siguiente pag", nextEmpresas);
    } catch (error) {
      console.error(error);
    }
  };
   
  useEffect(() => {
    checkNext();
    fetchEmpresas();
  }, [page, size, nextPage]);

  const handleNext= (event) =>{
    setLoading(true);
    setPage(page + 1);
    setNext(nextPage + 1);
    console.log("pagina siguiente", page);
    console.log("pagina siguiente Next", nextPage);
  }

  const handleLast= (event) =>{
    setLoading(true);
    setPage(page-1);
    setNext(nextPage - 1);
    console.log("pagina anterior", page);
  }

  return(
    <div>
    <Typography variant="h5" color="initial" align="center" style={{marginBottom: '15px'}}>
       Lista de Empresas
    </Typography>
  
    {loading ? (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '200px' }}>
        <CircularProgress color="primary" />
      </div>
    ) : (
      <div>
      <Grid container spacing={4} style={{ minWidth: '33.33%' }}>
        {empresas.map((empresa) => (
          <Grid item xs={12} sm={6} md={4} key={empresa.id} >
            <CardComponent 
              id={empresa.id}
              nombre={empresa.nombre} 
              descripcion={empresa.descripcion} 
              imagen={empresa.imagen}
            />
          </Grid>
        ))}
      </Grid>
      <div style={{display:'flex', justifyContent: 'center'}}>
        <IconButton 
          aria-label="Last" 
          color="primary" 
          size="large" 
          onClick={handleLast} 
          disabled={page === 0}
        >
          <ArrowBackIosNewIcon fontSize="inherit"/>
        </IconButton>
        <IconButton 
          aria-label="Next" 
          color="primary" 
          size="large" 
          onClick={handleNext}
          disabled={nextEmpresas.length === 0}
        >
          <ArrowForwardIosIcon fontSize="inherit"/>
        </IconButton>
      </div>
      </div>
      )
    }
    </div>
    
  );
}

export default Read;