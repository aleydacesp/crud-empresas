import React from 'react';
import { useState } from 'react';
import {Tab, Tabs, Box} from '@mui/material';
import Create from './Create';
import Read from './Read';
import Delete from './Delete';

const TabComponent = () =>{
    const [value, setValue] = useState(0);

    const handleChange = (event, val) => {
        setValue(val)
    };

    return(
        
        <div>
            <Tabs style={{ marginTop: '64px' }} value={value} onChange={handleChange}>
                <Tab label="CREAR" />
                <Tab label="VER"  />
                <Tab label="EDITAR" />
                <Tab label="ELIMINAR" />
            </Tabs>  
            <TabPanel value={value} index={0}>
                <Box 
                display="flex"
                justifyContent="center"
                height="100vh"
                >
                <Create/>
                </Box>
              
            </TabPanel>
            <TabPanel value={value} index={1}>
                <Box >
                <Read/>
                </Box>
            </TabPanel>
            <TabPanel value={value} index={2}>
              EDITAR EMPRESAS
            </TabPanel>
            <TabPanel value={value} index={3}>
              <Box >
                <Delete/>
              </Box>
            </TabPanel>

            
        </div>
    );
}

const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box p={3}>{children}</Box>}
      </div>
    );
  };
export default TabComponent;