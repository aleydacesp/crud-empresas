import React from 'react';
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'

const Navbar = ()=>{
    return(
        <div>
        
            <AppBar component="nav" position="fixed" color="primary">
              <Toolbar>
                <Typography variant="h6">
                  CRUD Empresas
                </Typography>
              </Toolbar>
            </AppBar>
        </div>
    );
}
export default Navbar;