import React from 'react';
import { Typography, Card, CardMedia, CardContent, CardActions, Button} from '@mui/material';

const CardComponent = ({id, nombre, descripcion, imagen}) => {
  const img = `data:image/jpeg;base64,${imagen}`;
  return(
    <div>
      <Card key={id} sx={{maxWidth:'400px'}}>
          <CardMedia component="img" height="140" image={img} />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {nombre}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {descripcion}
            </Typography>
          </CardContent>
        </Card>
    </div>
  );
}

export default CardComponent;