import React, { useState } from 'react';
//import axios from 'axios';
import { TextField, Button, Typography, Alert, Grid } from '@mui/material';

const Create = () =>{
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState(null);
  const [labelImage, setLabel] = useState('Ningun archivo seleccionado');
  const [messageSuccess, setMessageS] = useState('');
  const [messageError, setMessageE] = useState('');

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleImageChange = (event) => {
    const file = event.target.files[0];
    setImage(file);
    setLabel(file.name);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const formData = new FormData();
    formData.append('nombre', name);
    formData.append('descripcion', description);
    formData.append('imagen', image);


    fetch('http://35.238.246.148:8023/entereza/nuevaEmpresa', {
      method: 'POST',
      body: formData
    })
      .then(response => response.json())
      .then(data => {
        //empresa creada
        setMessageS(data.msg);
        setName('');
        setDescription('');
        setImage('');
        setLabel('Ningun archivo seleccionado');
      })
      .catch(error => {
        // Error al crear
        setMessageE(error);
      });
  };


  const handleReset = (event) =>{
    setName('');
    setDescription('');
    setImage('');
    setLabel('Ningun archivo seleccionado');
  }

  return (
      
    <form onSubmit={handleSubmit}>
      <Typography variant="h5" color="initial" align="center">
        Crear Empresa
      </Typography>
      {messageSuccess && <Alert severity="success">{messageSuccess}</Alert>}
      {messageError && <Alert severity="error">{messageError}</Alert>}
      <div>
        <TextField
        margin="normal"
        label="Nombre"
        size="small"
        sx={{ m: 1 }}
        fullWidth
        value={name}
        onChange={handleNameChange}
        required
      />
      <TextField
        margin="normal"
        label="Descripción"
        size="small"
        fullWidth sx={{ m: 1 }}
        value={description}
        onChange={handleDescriptionChange}
        required
      />
      </div >
    
      <Grid container spacing={3}>
      <Grid item xs={12} sm={6} md={8} >
      <TextField
        disabled
        margin="normal"
        size="small"
        fullWidth sx={{ m: 1 }}
        label={labelImage}
      />
      </Grid>
      <Grid item xs={12} sm={6} md={4} >
      <Button
        sx={{ marginTop: '8px'}}
        variant="contained"
        component="label"
        fullWidth
      >
        Upload File
          <input
          type="file"
          onChange={handleImageChange}
          hidden
      /> 
      </Button>
      </Grid>
      </Grid>
      <div style={{display:'flex', justifyContent:'flex-end'}}>
      <Button variant="outlined" onClick={handleReset} sx={{ m: 2 }}>Cancelar</Button>
      <Button type="submit" variant="contained" color="primary" sx={{m:2}} >
      Crear
      </Button>
      </div>
    </form>
  );
}

export default Create;