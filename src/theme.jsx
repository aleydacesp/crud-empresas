import { createTheme } from '@mui/material';
const theme = createTheme({
    palette :{
        primary: {
            main: '#55509d'
        }
    
    }
})

export default theme;