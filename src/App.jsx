import React from 'react';
import Navbar from './components/Navbar';
import TabCom from './components/TabComponent';
import { ThemeProvider } from '@mui/material';
import theme from './theme';

function App() {
  
  return (
    <div>
      <ThemeProvider theme={theme}>
      <Navbar/>
      <TabCom/> 
      </ThemeProvider>
         
    </div>
  );
}

export default App;
